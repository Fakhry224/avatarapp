package com.example.avatarapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.example.avatarapp.ui.theme.AvatarAppTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AvatarAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Content()
                }
            }
        }
    }
}


@Composable
fun Content() {
    val checkedHairState = remember { mutableStateOf(true) }
    val checkedEyebrowState = remember { mutableStateOf(true) }
    val checkedMoustacheState = remember { mutableStateOf(true) }
    val checkedBeardState = remember { mutableStateOf(true) }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,

    ) {
        Text(
            text = "MrHead",
            fontSize = 40.sp,
            fontWeight = FontWeight.Bold,
            letterSpacing = 5.sp,
            modifier = Modifier.padding(bottom = 20.dp)

        )

        ConstraintLayout() {
            val (body, beard, eyebrow, eyes, hair, moustache) = createRefs()

            Image(
                painter = painterResource(id = R.drawable.body),
                contentDescription = "Body",
                modifier = Modifier
                    .constrainAs(body) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                    .height(400.dp)
            )

            Image(
                painter = painterResource(id = R.drawable.hair),
                contentDescription = "Hair",
                modifier = Modifier
                    .constrainAs(hair) {
                        top.linkTo(body.top)
                        start.linkTo(body.start)
                        end.linkTo(body.end)
                    }
                    .width(280.dp)
                    .height(150.dp)
                    .alpha(if (!checkedHairState.value) 0.0f else 1.0f)
            )

            Image(
                painter = painterResource(id = R.drawable.eyebrow),
                contentDescription = "Eyebrow",
                modifier = Modifier
                    .constrainAs(eyebrow) {
                        top.linkTo(body.top)
                        start.linkTo(body.start)
                        end.linkTo(body.end)
                    }
                    .padding(top = 110.dp)
                    .width(100.dp)
                    .height(40.dp)
                    .alpha(if (!checkedEyebrowState.value) 0.0f else 1.0f)
            )

            Image(
                painter = painterResource(id = R.drawable.eyes),
                contentDescription = "Eyes",
                modifier = Modifier
                    .constrainAs(eyes) {
                        top.linkTo(eyebrow.bottom)
                        start.linkTo(body.start)
                        end.linkTo(body.end)
                    }
            )

            Image(
                painter = painterResource(id = R.drawable.beard),
                contentDescription = "Beard",
                modifier = Modifier
                    .constrainAs(beard) {
                        bottom.linkTo(body.bottom)
                        start.linkTo(body.start)
                        end.linkTo(body.end)
                    }
                    .padding(bottom = 40.dp)
                    .width(160.dp)
                    .alpha(if (!checkedBeardState.value) 0.0f else 1.0f)
            )

            Image(
                painter = painterResource(id = R.drawable.moustache),
                contentDescription = "Moustache",
                modifier = Modifier
                    .constrainAs(moustache) {
                        top.linkTo(eyes.bottom)
                        start.linkTo(body.start)
                        end.linkTo(body.end)
                    }
                    .padding(top = 20.dp)
                    .alpha(if (!checkedMoustacheState.value) 0.0f else 1.0f)
            )
        }

        Row(
            Modifier
                .fillMaxWidth()
                .padding(top = 10.dp),
            horizontalArrangement = Arrangement.SpaceBetween,

        ) {
            Column(Modifier.padding(start = 20.dp)) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,
                    ) {
                    Checkbox(
                        checked = checkedHairState.value,
                        onCheckedChange = { checkedHairState.value = it },
                        modifier = Modifier.padding(5.dp),
                        colors = CheckboxDefaults.colors(checkedColor = Color.Red)
                    )
                    Text(
                        text = "Rambut",
                        modifier = Modifier.padding(end = 10.dp)
                    )
                }

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,

                    ) {
                    Checkbox(
                        checked = checkedMoustacheState.value,
                        onCheckedChange = { checkedMoustacheState.value = it },
                        modifier = Modifier.padding(5.dp),
                        colors = CheckboxDefaults.colors(checkedColor = Color.Red)
                    )
                    Text(
                        text = "Kumis",
                        modifier = Modifier.padding(end = 10.dp)
                    )
                }

            }

            Column(Modifier.padding(end = 20.dp)) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,

                    ) {
                    Checkbox(
                        checked = checkedEyebrowState.value,
                        onCheckedChange = { checkedEyebrowState.value = it },
                        modifier = Modifier.padding(5.dp),
                        colors = CheckboxDefaults.colors(checkedColor = Color.Red)
                    )
                    Text(
                        text = "Alis",
                        modifier = Modifier.padding(end = 10.dp)
                    )
                }

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center,

                    ) {
                    Checkbox(
                        checked = checkedBeardState.value,
                        onCheckedChange = { checkedBeardState.value = it },
                        modifier = Modifier.padding(5.dp),
                        colors = CheckboxDefaults.colors(checkedColor = Color.Red)
                    )
                    Text(
                        text = "Janggut",
                        modifier = Modifier.padding(end = 10.dp)
                    )
                }
            }
        }

    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Bottom,
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = 20.dp)
    ) {
        Text(text = "Nama : Fakhry Zahran Hakim")
        Text(text = "NIM : 225150401111005")
    }
}
